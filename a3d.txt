#!/bin/csh
#
# a3d : script to compute fiber angles based on the Matlab code from Liu et
# al., "Rapid three-dimensional quantification of voxel- wise collagen fiber
# orientation", Biom. Opt. Express, 2015.
#
# - Software requirements and packages
#
#     Except for Matlab, all software below is open source, freely available
#     for most platforms:
#
#     . Matlab: the core code to compute angles is written in Matlab
#
#     . bftools: BioFormats package. It provides 'bfconvert' which we use to
#     convert a CZI stack to a TIFF stack needed by the Matlab code
#
#     . gmic: we use to negate angle values in tiff stacks
#
#     . coreutils: provide core utility functions; we use 'realpath' to get
#     full path of files and directories
#
#     . util-linux: provide other utility functions; we use the enhanced
#     version of getopt
# 
#  
# TODO:
#   DONE - add -f, --folder flag to get folder where czis are and run for all files there (obs: CZI only!)
#   DONE - create histograms for all angles in given intervals - interval provided as input value
#   DONE - save also beta and gamma values, also in color
#   DONE - mask angle files with negative values for voxels out of mask and positive values for within mask
#
# Copyright (C) 2018 Alexandre Cunha, Caltech
#

set start = `date +%s`
set verbosity = false

#
# Modify the variables below accordingly and in case these executables are not in your PATH
#

set A3D_folder = /home/cunha/private/computable-plant/pauline/A3D/A3D

set matlab = /home/cunha/bin/matlab
set bfconvert = /home/cunha/bin/bfconvert
set gmic = /opt/local/bin/gmic

#
# usage message
#

set msg = "\n\t\033[1m usage: `basename $0` [options] <folder | stack.[czi,tif,tiff]>\033[0m \n\n"
set msg = "$msg options are:\n"
set msg = "$msg \t -a, --angle <int>\t angle interval for histogram binning, in degrees - (1)\n" 
set msg = "$msg \t -c, --conn <int>\t number of connections per voxel in 3D: 6,14,18,26 - (18)\n"
set msg = "$msg \t -d, --outdir <string>\t directory for output - (same as input)\n"
set msg = "$msg \t -g, --histograms \t toogle computation of angle histograms - (off)\n"
set msg = "$msg \t -h, --help \t\t prints out this message\n"
set msg = "$msg \t -m, --mask <file.tif>\t B&W mask for fibers in tiff format - (none)\n"
set msg = "$msg \t -o, --output <string>\t prefix for output files - (same as input)\n"
set msg = "$msg \t -p, --param <filename>\t name of file with parameter values - (default is A3D.options)\n"
set msg = "$msg \t -r, --voxelratio <float> ratio of voxel size, i.e. size of z over size in x direction - (1.0)\n"
set msg = "$msg \t -R, --recursive \t process all CZI files within and under the given folder - (not recursive)\n"
set msg = "$msg \t -s, --meanratio <float> ratio of overall mean intensity to set background - (0.45)\n"
set msg = "$msg \t -t, --threshold <int>\t threshold value for background - (10)\n"
set msg = "$msg \t -v, --minvol <int>\t volume threshold to remove small objects - (15)\n"
set msg = "$msg \t -V, --verbose \t\t toggle verbosity on - (off)\n"
set msg = "$msg \t -x, --armdx <int>\t size in pixels of orientation window in x and y directions (approx radius of fiber) - (4)\n"
set msg = "$msg \t -X, --armdxx <int>\t size in pixels of 3D directional variance window in x and y directions - (4)\n"
set msg = "$msg \t -z, --armdz <int>\t size in pixels of orientation window in z direction (approx radius of fiber) - (2)\n"
set msg = "$msg \t -Z, --armdzz <int>\t size in pixels of 3D directional variance window in z direction - (2)\n"
set msg = "$msg \n"
set msg = "$msg\033[32m Note: command line values supersede option values found in parameter file,\n"
set msg = "$msg if provided via -p or --param, and in the default A3D.options file.\033[0m\n\n"
set msg = "$msg Copyright (C) 2018, Alexandre Cunha, Caltech\n\n"

#
# check if bare minimum is provided or help is requested
#

echo $argv | grep -w -e "-h" -e "--help" > /dev/null
if ( $? == 0 || $#argv < 1 ) then
    set emsg = "$msg"
    goto EXIT_OK
endif

#
# parse command line
#

set cmdline = (`getopt -s csh -o a:c:d:ghm:o:p:r:s:Rt:v:Vx:X:z:Z: --long angle:,conn:,outdir:,help,histogram,mask:,output:,param:,voxelratio:,recursive,threshold:,meanratio:,minvol:,verbose,armdx:,armdxx:,armdz:,armdzz: -- $argv:q`)

if ( $? != 0 ) then
  set emsg = "error: getopt probably not available in the system or older version (?)"
  goto EXIT_ERROR
endif

eval set argv = \($cmdline:q\)

# options given in the command line will be written in this temporary file
set opt = `mktemp -q /tmp/A3D_options_XXXXXX`

set hangle = 1

while (1)
    switch("$1:q")
    case -a:
    case --angle:
        set hangle = "$2:q"
        if ( `echo $hangle | tr -d \[:digit:\]` != "" ) then
            set emsg = "histogram angle bin must be an integer number in interval [1,90]; given $hangle"
            goto EXIT_ERROR
        endif
        if ( $hangle < 1 || $hangle > 90 ) then
            set emsg = "histogram angle bin must be an integer number in interval [1,90]; given $hangle"
            goto EXIT_ERROR
        endif
        shift; shift
        echo "angle_bin $hangle" >> $opt
        breaksw
    case -c:
    case --conn:
        set connectivity = "$2:q"
        if ( `echo $connectivity | tr -d \[:digit:\]` != "" ) then
            set emsg = "connectivity must be a number, one of 6, 14, 18, 26; given $connectivity."
            goto EXIT_ERROR
        endif
        if ( `echo "6 14 18 26" | grep -w $connectivity` == "" ) then
            set emsg = "connectivity must be one of 6, 14, 18, 26; given $connectivity."
            goto EXIT_ERROR
        endif
        shift; shift
        echo "connectivity $connectivity" >> $opt
        breaksw
    case -d:
    case --outdir:
        set outdir = `realpath -z "$2:q"`
        if ( ! -d $outdir ) then
            mkdir -p $outdir
        endif
        if ( $? != 0 ) then
            set emsg = "could not create output directory $outdir."
            goto EXIT_ERROR
        endif
        echo "output_dir $outdir" >> $opt
        shift; shift
        breaksw
    case -g:
    case --histogram:
        set histogram = true
        echo "histogram 1" >> $opt
        shift
        breaksw
    case -m:
    case --mask:
        set mask = `realpath -z "$2:q"`
        if ( ! -r $mask ) then
            set emsg = "mask file $mask cannot be read."
            goto EXIT_ERROR
        endif
        echo "mask $mask" >> $opt
        shift; shift
        breaksw
    case -o:
    case --output:
        set output = "$2:q"
        echo "output_prefix $output" >> $opt
        shift; shift
        breaksw
    case -p:
    case --param:
        set param = `realpath -z "$2:q"`
        if ( ! -r $param ) then
            set emsg = "cannot read parameters file $param."
            goto EXIT_ERROR
        endif
        shift; shift
        breaksw
    case -r:
    case --voxelratio:
        set voxel_ratio = `echo "$2:q" | sed 's/^0*//'`
        set tmp = `echo $voxel_ratio | tr -d \[:digit:\]`
        if ( $#tmp > 1 || ($#tmp == 1 && $tmp != '.' )) then
            set emsg = "voxel ratio must be a number; given $voxel_ratio."
            goto EXIT_ERROR
        endif
        echo "voxel_size_ratio $voxel_ratio" >> $opt
        shift; shift
        breaksw
    case -R:
    case --recursive:
        set recursive = true
        echo "descend_recursive 1" >> $opt
        shift
        breaksw
    case -s:
    case --meanratio:
        set meanratio = `echo "$2:q" | sed 's/^0*//'`
        set tmp = `echo $meanratio | tr -d \[:digit:\]`
        if ( $#tmp > 1 || ($#tmp == 1 && $tmp != '.' )) then
            set emsg = "meanratio must be a number; given $meanratio."
            goto EXIT_ERROR
        endif
        echo "threshold_ratio $meanratio" >> $opt
        shift; shift
        breaksw
    case -t:
    case --threshold:
        set threshold = `echo "$2:q" | sed 's/^0*//'`
        set tmp = `echo $threshold | tr -d \[:digit:\]`
        if ( $#tmp > 0 ) then
            set emsg = "background threshold must be an integer number: $threshold is not valid."
            goto EXIT_ERROR
        endif
        echo "threshold_value $threshold" >> $opt
        shift; shift
        breaksw
    case -v:
    case --minvolume:
        set minvol = `echo "$2:q" | sed 's/^0*//'`
        set tmp = `echo $minvol | tr -d \[:digit:\]`
        if ( $#tmp > 0 ) then
            set emsg = "minimum volume for connected component must be an integer number; given $minvol."
            goto EXIT_ERROR
        endif
        echo "min_volume $minvol" >> $opt
        shift; shift
        breaksw
    case -V:
    case --verbose:
        set verbosity = true
        shift
        breaksw
    case -x:
    case --armdx:
        set armdx = "$2:q"
        if ( `echo $armdx | tr -d \[:digit:\]` != "" ) then
            set emsg = "orientation window size in x and y must be an integer number; given $armdx."
            goto EXIT_ERROR
        endif
        if ( $armdx < 1 || $armdx > 100 ) then
            set emsg = "orientation window size in x and y must be an integer in interval [1,100]; given $armdx."
            goto EXIT_ERROR
        endif
        shift; shift
        echo "armdx $armdx" >> $opt
        breaksw
    case -X:
    case --armdxx:
        set armdxx = "$2:q"
        if ( `echo $armdxx | tr -d \[:digit:\]` != "" ) then
            set emsg = "variance window size in x and y must be an integer number; given $armdxx."
            goto EXIT_ERROR
        endif
        if ( $armdxx < 1 || $armdxx > 100 ) then
            set emsg = "variance window size in x and y must be an integer in interval [1,100]; given $armdxx."
            goto EXIT_ERROR
        endif
        shift; shift
        echo "armdxx $armdxx" >> $opt
        breaksw
    case -z:
    case --armdz:
        set armdz = "$2:q"
        if ( `echo $armdz | tr -d \[:digit:\]` != "" ) then
            set emsg = "orientation window size in z direction must be an integer number; given $armdz."
            goto EXIT_ERROR
        endif
        if ( $armdz < 1 || $armdz > 100 ) then
            set emsg = "orientation window size in z direction must be an integer in interval [1,100]; given $armdz."
            goto EXIT_ERROR
        endif
        shift; shift
        echo "armdz $armdz" >> $opt
        breaksw
    case -Z:
    case --armdzz:
        set armdzz = "$2:q"
        if ( `echo $armdzz | tr -d \[:digit:\]` != "" ) then
            set emsg = "variance window size in z direction must be an integer number; given $armdzz."
            goto EXIT_ERROR
        endif
        if ( $armdzz < 1 || $armdzz > 100 ) then
            set emsg = "variance window size in z direction must be an integer in interval [1,100]; given $armdzz."
            goto EXIT_ERROR
        endif
        shift; shift
        echo "armdzz $armdzz" >> $opt
        breaksw
    case --:
        shift
        break
    default:
        set emsg = "internal error parsing command line. notify developer."
        goto EXIT_ERROR
    endsw
end

if ( $#argv > 1 ) then
    set emsg = "number of input files or folders exceeds allowed, $argv."
    goto EXIT_ERROR
endif

#
# determine if input is a folder or a image stack, assumed to be last entry in the command line
#

set input = `realpath -z "$argv[1]:q"`
if ( -d $input ) then
    set imgdir = $input
    if ( $?recursive ) then
        set files = (`find $input -type f -iname "*.czi" -exec realpath {} \;`)
    else
        set files = (`find $input -maxdepth 1 -type f -iname "*.czi" -exec realpath {} \;`)
    endif
    if ( $#files == 0 ) then
        set emsg = "could not find CZI files from $input. nothing to process."
        goto EXIT_ERROR
    endif
else
    # a single stack to process
    set files = $input
    if ( ! -r $files ) then
        set emsg = "cannot read image stack. verify file $files"
        goto EXIT_ERROR
    endif
    set imgdir = $files:h
endif

#
# determine default values for variables and running parameters; these must be
# in a file called A3D.options located, in order of preference, in the same
# input folder, in the home directory, or current directory.
#

if ( -r $imgdir/A3D.options ) then
    set DEFAULT_OPTIONS = $imgdir/A3D.options
else
    if ( -r $HOME/A3D.options ) then
        set DEFAULT_OPTIONS = $HOME/A3D.options
    else
        if ( -r $PWD/A3D.options ) then
            set DEFAULT_OPTIONS = $PWD/A3D.options
        endif
    endif
endif

if ( $?DEFAULT_OPTIONS ) then
    cat $DEFAULT_OPTIONS | awk '{ print "setenv A3D_"toupper($1)" "$2 }' | source /dev/stdin
else
    set emsg = "a default options file A3D.options was not found. Provide one in the input folder, home directory, or in the directory used to launch this program."
    goto EXIT_ERROR
endif


$verbosity && printf "-------------------------------------------------------------------------------------------------------\n" > /dev/stdout 
$verbosity && printf "\t A3D - $USER - `date +%c` \n" > /dev/stdout
$verbosity && printf "-------------------------------------------------------------------------------------------------------\n" > /dev/stdout 
$verbosity && printf "\t processing $#files images \n" > /dev/stdout
$verbosity && printf "-------------------------------------------------------------------------------------------------------\n" > /dev/stdout 


#
# read in parameters from file and provided in the command line; command line
# values supersede those in the parameter file
#

if ( $?param ) then
    cat $param | awk '{ print "setenv A3D_"toupper($1)" "$2 }' | source /dev/stdin 
endif

if ( ! -z $opt ) then
    cat $opt | awk '{ print "setenv A3D_"toupper($1)" "$2 }' | source /dev/stdin
endif

# if output dir and prefix were never defined, use ones corresponding to input
if ( ! $?A3D_OUTPUT_DIR ) then
    setenv A3D_OUTPUT_DIR $imgdir
endif

# make sure A3D executables are found on the execution path
setenv PATH ${A3D_folder}:${PATH}
setenv A3D_DIR $A3D_folder

#
# compute angles for each stack
#

@ remain = $#files
@ counter = 0

foreach img ( $files )

    @ counter++
    $verbosity && printf "%3d : $img\n" $counter > /dev/stdout

    set tic1 = `date +%s`
    setenv A3D_IMAGE $img
    
    if ( $#files > 1 ) then
        setenv A3D_OUTPUT_PREFIX $img:t:r
    else
        if ( ! $?A3D_OUTPUT_PREFIX ) then
            setenv A3D_OUTPUT_PREFIX $img:t:r
        endif
    endif
    set img_prefix = $img:r

    set format = `echo $img:e | tr "[a-z]" "[A-Z]"`
    if ( $format == 'CZI' ) then
        # Get CZI information and convert to a single channel TIFF stack
        set imginfo = `czi_info $img`
    else
        if ( $format == 'TIF' || $format == 'TIFF' ) then
            set imginfo = `tif_info $img`
            set tif = $img
        else
            set emsg = "can only recognize TIF and CZI images ($format not allowed)."
            goto EXIT_ERROR
        endif
    endif
    
    if ( $? != 0 ) then
        set emsg = "could not determine metadata information for file $img."
        goto EXIT_ERROR
    endif

    # OBS: expected output of czi_info or tif_info to be in the order given
    # below. Note: Modify if this is no longer valid
    #
    #  [x_size] [y_size] [z_size] [width] [height] [#slices] [#channels] [bit depth]
    #
    
    set size_x = $imginfo[1]                    # voxel size in x
    set size_y = $imginfo[2]                    # voxel size in y
    set size_z = $imginfo[3]                    # voxel size in z
    set width  = $imginfo[4]                    # width
    set height = $imginfo[5]                    # height
    set depth  = $imginfo[6]                    # number of slices
    set size_c = $imginfo[7]                    # number of channels
    set bits   = $imginfo[8]                    # number of valid bits

    # set voxel size ratio, i.e., z/xy sizes; this is data dependent
    if ( `expr $size_x \> 0` ) then
        set voxel_ratio = `echo "scale =6; $size_z/$size_x" | bc -l`
    endif
    
    #
    # create TIFF stack if not provided, needed by matlab code
    #
    
    if ( $format == 'CZI' ) then
        set tif = $img_prefix.tif
        if ( ! -r $tif ) then

            $verbosity && printf "\t converting stack to TIFF format " > /dev/stdout
            set tic = `date +%s`
            
            if ( $size_c > 1 ) then
                # NOTE: by default, 1st channel is the one of interest. Change if necessary!
                $bfconvert -no-upgrade -compression LZW -overwrite -channel 0 -option zeissczi.attachments false $img $tif > /dev/null
            else
                $bfconvert -no-upgrade -compression LZW -overwrite -option zeissczi.attachments false $img $tif > /dev/null
            endif

            if ( ! -r $tif ) then
                set emsg = "could not convert $img to a TIFF stack."
                goto EXIT_ERROR
            endif

            set toc = `date +%s`
            $verbosity && printf "(%d seconds)\n" `expr $toc - $tic` > /dev/stdout 
        endif
    endif
    
    #
    # set up remaining environment variables for MATLAB
    #
    
    if ( $?voxel_ratio ) then
        setenv A3D_VOXEL_SIZE_RATIO $voxel_ratio
    endif
    
    setenv A3D_TIFF $tif
    env | grep "A3D_" | sort | sed 's/=/\ /g' > $A3D_OUTPUT_DIR/$A3D_OUTPUT_PREFIX.parameters.log
    
    set matlab_logname = $A3D_OUTPUT_DIR/$A3D_OUTPUT_PREFIX.matlab.`date '+%F-%T'`.log
    
    #------------------------------------------------------
    # entering matlab
    #------------------------------------------------------

    $verbosity && printf "\t computing angles, this may take a while " > /dev/stdout
    set tic = `date +%s`
        
    # call matlab in batch mode, no screen, no java, no X11,...
    nohup $matlab -nodisplay -nosplash -nojvm -nodesktop >&! $matlab_logname <<THE_END
    
    tic
    
    global options;
    
    options.a3d_path = getenv('A3D_DIR');
    addpath( options.a3d_path, '-begin' );
    
    readoptions();
    
    stack_name = getenv('A3D_TIFF');
    stack = loadtiff( stack_name ); 
    
    mask_name = getenv('A3D_MASK');
    if isempty( mask_name )
        mask = mean_threshold( stack );
    else
        mask = logical( loadtiff( mask_name ));
    end

    [phi, theta, beta, gamma] = fiberangles3D( double(stack), mask );
    
    toc
    
THE_END

    #------------------------------------------------------
    # done with matlab
    #------------------------------------------------------

    set toc = `date +%s`
    $verbosity && printf "(%d seconds)\n" `expr $toc - $tic` > /dev/stdout 

    #
    # negate angle values in angle files (phi, theta, beta, gamma) which are
    # outside segmentation mask
    #

    $verbosity && stdbuf -oL printf "\t negating angle values outside segmentation mask " > /dev/stdout
    set tic = `date +%s`
    
    set rootname = $A3D_OUTPUT_DIR/$A3D_OUTPUT_PREFIX
    set phi = $rootname.phi.tif
    set theta = $rootname.theta.tif
    set beta = $rootname.beta.tif
    set gamma = $rootname.gamma.tif
    set angle_files = ( $phi $theta $beta $gamma )

    if ( ! $?A3D_MASK ) then
        set mask = $rootname.mask.tif
    endif

    if ( -r $mask ) then
        set aux = `mktemp -u /tmp/A3D_XXXXX.tif`
        foreach f ( $angle_files )
            if ( -r $f ) then
                $gmic -v -1 $f -append z -input $mask -append\[1--1\] z -normalize\[1\] -1,1 -mul -round 1,0 -o $aux,short,lzw
                mv $aux $f
            else
                printf "\n\t warning: could not read angle file $f.\n" > /dev/stdout
            endif
        end
    endif

    set toc = `date +%s`
    $verbosity && printf "(%d seconds)\n" `expr $toc - $tic` > /dev/stdout 


    #
    # compute histograms
    #

    if ( $?histogram ) then
        $verbosity && stdbuf -oL printf "\t computing histogram of angle files" > /dev/stdout
        set tic = `date +%s`

        foreach f ( $angle_files )
          if ( -r $f ) then
            gmic -v -1 $f -append z -o -.asc | awk -v bin=$hangle 'BEGIN { getline } { for (i=1;i<=NF;i++) a[int($i/bin)]++ } END { for (h in a) if (h >= 0) print bin*h,a[h] }' | sort -g -k1 > $f:r.hst
          endif
        end

        set toc = `date +%s`
        $verbosity && printf "(%d seconds)\n" `expr $toc - $tic` > /dev/stdout 
    endif


    @ remain--
    set toc = `date +%s`
    set etime = `echo "scale=0; ($toc - $tic1)/60; ($toc - $tic1)%60" | bc -l`
    $verbosity && printf "\t completed work for $img:t in $etime[1]min:$etime[2]sec\n"  > /dev/stdout
    $verbosity && printf "\t remaining stacks: $remain\n"  > /dev/stdout
    $verbosity && printf "-------------------------------------------------------------------------------------------------------\n" > /dev/stdout 
end

#
# clean up
#

rm -f $opt

set toc = `date +%s`
$verbosity && printf " approximate total ellapsed time: %.2lf minutes\n" `echo "scale=2; ($toc - $start)/60" | bc -l` > /dev/stdout 
$verbosity && printf "-------------------------------------------------------------------------------------------------------\n" > /dev/stdout 

#
# exit points
#

EXIT_OK:
    if ( $?emsg ) then
        printf "$emsg\n" > /dev/stdout
    endif
    exit 0

EXIT_ERROR:
    if ( $?emsg ) then
        printf "error: $emsg\n" > /dev/stderr
    endif
    exit 1

    
